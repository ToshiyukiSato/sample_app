require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  def setup
    @relationship = Relationship.new(follower_id: users(:testman).id,
                                     followed_id: users(:toastman).id)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test "should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end

  test "should follow and unfollow a user" do
    testman = users(:testman)
    toastman = users(:toastman)
    assert_not testman.following?(toastman)
    testman.follow(toastman)
    assert testman.following?(toastman)
    assert toastman.followers.include?(testman)
    testman.unfollow(toastman)
    assert_not testman.following?(toastman)
  end
end
